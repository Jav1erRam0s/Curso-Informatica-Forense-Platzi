# Curso-Informatica-Forence-Platzi

Curso de Informatica Forence en Platzi.

## 1. Bienvenida y presentación general

* 1. Introducción y bienvenida
* 3. Etapas I y II Identificación y Preservación de Evidencia Digital
* 4. Etapas III y IV Análisis y Presentación de Resultados

## 2. Etapa I Identificación

* 1. Preparación de un kit para adquisición
* 2. Procedimientos de Cadena de Custodia
* 3. Identificación de fuentes de evidencia
* 4. Elaborar un inventario de evidencia y plan de adquisiciones en un escenario simulado (Doc.)

## 3. Etapa II Preservación

* 1. Sistemas de protección contra escritura por hardware y software
* 2. Introducción a FTK Imager
* 3. Adquisición de imágenes forenses con FTK Imager
* 4. Adquisición de imágenes forenses con EnCase
* 5. Adquisición de imágenes lógicas, memoria volátil y sistemas operativos en vivo (Doc.)
* 6. Introducción a Paladin Forensics
* 7. Adquisición de imágenes forenses con DD
* 8. FTK Imager para Linux
* 9. Adquisición de imágenes forenses de Mac. (Paladin)
* 10. Verificación de imágenes forenses. Explicación de algoritmos Hash
* 11. Adquisición de imágenes forenses de Mac. (Target Disk Mode)
* 12. Adquirir imágenes forenses, utilizando las diferentes herramientas disponibles (Doc.)
* 13. Ejecutar la verificación con algoritmos HASH de las diferentes imágenes forenses generadas (Doc.)

## 4. Etapa III Análisis de Evidencia Pt. 1

* 1. Sistemas de archivos Windows FAT, NTFS
* 2. Sistemas de archivos Unix EXT, HFS y APFS
* 3. Exportado de archivos a partir de imágenes forenses
* 4. Creación de imágenes parciales con FTK Imager
* 5. Análisis preliminar de sistemas Windows
* 6. Análisis preliminar de sistemas Unix (Linux y MacOS)
* 7. Elaboración de informe preliminar (Doc.)
* 8. Crear una imagen de contenido personalizado para análisis (Doc.)
* 9. Elaborar un informe de análisis preliminar de un entorno a partir de la evidencia preservada (Doc.)

## 5. Etapa III Análisis de Evidencia Pt. 2

* 1. Análisis de Registro de Windows SAM
* 2. Análisis de Registro de Windows Software
* 3. Análisis de logs de un Sistema Windows
* 4. Análisis de listas recientes (MRU) y Shellbags
* 5. Referencia del registro de Windows (Doc.)
* 6. Análisis de procesos ejecutados
* 7. Análisis de bandejas de reciclaje
* 8. Elaborar un informe a partir de los resultados obtenidos en cada una de las etapas de análisis (Doc.)

## 6. Etapa III Análisis de Evidencia Pt. 3

* 1. Estructura de un sistema Unix
* 2. Arranque y ejecución de procesos en Linux (Doc.)
* 3. Análisis de archivos de autenticación
* 4. Análisis temporal del sistema
* 5. Autopsy y Sleuth Kit Suite
* 6. Elaborar un informe a partir de los resultados obtenidos en cada una de las etapas de análisis (Doc.)

## 7. Etapa IV Presentación

* 1. Elaboración de un informe ejecutivo
* 2. Elaboración de un informe técnico completo
* 3. Presentación ante autoridades judiciales
* 4. Consolidar los resultados en dos informes técnico y ejecutivo, empleando el lenguaje adecuado para cada caso (Doc.)

## 8. Conclusiones Finales

* 1. Recomendaciones generales, resumen y sugerencias de material.  Despedida y agradecimiento

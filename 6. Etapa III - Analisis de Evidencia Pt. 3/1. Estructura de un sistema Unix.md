
---
# **Estructura de un sistema Unix**

* Diferencias con Windows
    - Configuracion independientes por cada proceso. No centralizado.
    - La mayoria de archivos de configuracion estan en texto plano.
    - Varia entre distribuciones y versiones.

* Tres aspectos de linux interesantes para analizar.
    - /home/$user : Informacion y configuracion de cada usuario.
    - /etc : Configuracion general del sistema y de aplicaciones.
    - /var/log : Registro de actividad del sistema operativo y los usuarios.

## **Estructura de particiones**

![Estructura de particiones](img/estructura-de-particiones.png "EP")

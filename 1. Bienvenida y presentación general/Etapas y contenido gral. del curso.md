
---
## **Etapas I y II Identificación y Preservación de Evidencia Digital**
---

**Identificacion** : Detectar, reconocer y determinar las fuentes de informacion que deben ser preservadas para una investigacion.

* 6 preguntas : ¿ Como aplicar las  6 del procesos investigativo ?
* Preparacion de herramientas : Un kit de herramientas adecuadas para hacer adquisiciones.
* Primer respondiente : ¿ Quien es y que debe hacer ?
* Toma de decisiones : Adquisiciones "en vivo", "estaticas".

* Resultado de la etapa I (Identificacion)
    - Cadena de custodia
    - Inventario de fuentes

**Preservacion** : Recoleccion de informacion de dispositivos de almacenamiento de datos, para generar copias exactas usando tecnicas forences.

* Medios de almacenamientos : ¿ Que son ? ¿ Cuales usamos comunmente ? ¿ Como funcionan ?
* ¿ Que es una imagen forence ? : Defincion, ejemplos y usos comunes.
* Adquisicion de imagenes forences : FTK Imager, Paladin Forensics, EnCaseImager, entro otras tecnicas.
* Algoritmos Hash : ¿ Que son y por que los utilizamos ?

* Resultados de la etapa II (Preservacion)
    - Imagenes forences
    - Reportes de adquisicion y verificacion

---
## **Etapas III y IV Análisis y Presentación de Resultados**
---

**Analisis** : Procesamiento de informacion relacionada con el objetivo de la investigacion, con el fin de determinar hechos asosciados con un evento.

* Analisis Preliminar : lo que se hace es un analisis rapido, eficiente de la informacion que ya copiamos, de los medios e imagenes que tenemos para darle prioridad.
* Analisis de Sistemas Windows.
* Analisis de Sistemas Unix.

* Sistemas de archivos : ¿ Que son ? ¿ Cuales usamos comunmente ? ¿ Como funcionan ?
* Creacion de imagenes parciales : Clasificacion de datos para analisis.
* Exportado de archivos : Separacion de informacion.
* Analisis preliminar de Sistemas Operativos : Windows y Unix.

* Resultados del analisis preliminar
    - Informacion filtrada para analisis
    - Reporte preliminar de analisis

* Analisis de usuario de sistema : ¿ Que usuarios han estado activos en el sistema ?
* Analisis de logs y procesos ejecutados : Reconstruccion de la actividad del Sistema Operativo.
* Recuperacion de archivos borrados : Reconstruccion y procesos de data carving.
* Artefactos especificos de sistema : Diferencias entre Windows y Unix.

* Resultados del analisis general
    - Resultados generales de la investigacion
    - Posibles fuentes adicionales de consulta

**Presentacion** : Entrega de resultados, con un lenguaje adecuado, en forma de reportes a las partes interesadas o autoridades que los requieran.

* Organizacion de la informacion : ¿ Que debemos incluir y que no ? ¿ Como presentar los resultados y que decir ? ¿ Que detalles debemos incluir ?
* Creacion de un informe tecnico : ¿ Que detalles debemos incluir ?
* Creacion de un informe ejecutivo : ¿ Que lenguaje debemos utilizar ? ¿ Como resumir nuestros hallazgos ?
* Presentacion ante autoridades : Consejos y recomendaciones.

* Resultados de la presentacion
    - Informe Tecnico
    - Informe Ejecutivo